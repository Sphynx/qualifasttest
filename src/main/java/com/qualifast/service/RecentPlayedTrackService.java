package com.qualifast.service;

import com.qualifast.entity.Song;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
public class RecentPlayedTrackService {

    private final ProfileDetailService profileDetailService;

    private final SongService songService;

    private final RestTemplate restTemplate;
    private static final String URL = "https://api.spotify.com/v1/me/player/recently-played?limit=30";

    public Object getHistory(String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + token);

        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

        ResponseEntity<Object> response = restTemplate.exchange(URL, HttpMethod.GET, entity, Object.class);
        LinkedHashMap result = (LinkedHashMap) response.getBody();

        saveSongs(result, profileDetailService.getSpotifyUserId(token));

        return result;
    }

    private void saveSongs(LinkedHashMap resultMap, String spotifyUserId) {
        ArrayList<LinkedHashMap> items = (ArrayList<LinkedHashMap>) resultMap.get("items");
        for (LinkedHashMap trackMap : items) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            LocalDateTime playedAt = LocalDateTime.parse((String) trackMap.get("played_at"), formatter);
            LinkedHashMap track = (LinkedHashMap) trackMap.get("track");
            String songId = (String) track.get("id");
            String songName = (String) track.get("name");
            ArrayList<LinkedHashMap> artists = (ArrayList<LinkedHashMap>) track.get("artists");
            String songArtistId = (String) artists.get(0).get("id");
            String songArtistName = (String) artists.get(0).get("name");

            //This is preventing for duplication records
            if (songService.findBySpotifyUserIdAndSpotifySongIdAndPlayedAt(spotifyUserId, songId, playedAt)
                    .isPresent()) {
                continue;
            }
            Song song = new Song(null, songName, songArtistId, songArtistName, spotifyUserId, songId, playedAt);
            songService.save(song);
        }
    }
}
