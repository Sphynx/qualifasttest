package com.qualifast.service;

import com.qualifast.entity.Song;
import com.qualifast.repository.SongRepository;
import java.time.LocalDateTime;
import java.util.Optional;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SongService {

    private final SongRepository songRepository;

    public Optional<Song> findBySpotifyUserIdAndSpotifySongIdAndPlayedAt(String spotifyUserId, String spotifySongId,
            LocalDateTime playedId) {
        return songRepository.findBySpotifyUserIdAndSpotifySongIdAndPlayedAt(spotifyUserId, spotifySongId, playedId);
    }

    @Transactional
    public Song save(Song song) {
        return songRepository.save(song);
    }

    public void deleteById(Long id) {
        songRepository.deleteById(id);
    }
}
