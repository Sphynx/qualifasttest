package com.qualifast.service;

import com.qualifast.entity.User;
import java.util.LinkedHashMap;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
public class ProfileDetailService {

    private final UserService userService;

    private final RestTemplate restTemplate;
    private static final String URL = "https://api.spotify.com/v1/me";

    public LinkedHashMap getUser(String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + token);

        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);

        ResponseEntity<Object> response = restTemplate.exchange(URL, HttpMethod.GET, entity, Object.class);
        LinkedHashMap result = (LinkedHashMap) response.getBody();

        checkAndSaveUser(result);

        return result;
    }

    private void checkAndSaveUser(LinkedHashMap userMap) {
        String spotifyId = (String) userMap.get("id");

        if (!userService.findBySpotifyId(spotifyId).isPresent()) {
            User user = new User(null, (String) userMap.get("display_name"), (String) userMap.get("email"),
                    spotifyId, null);
            userService.save(user);
        }
    }

    public String getUsername(String token) {
        LinkedHashMap user = getUser(token);
        return (String) user.get("display_name");
    }

    public String getSpotifyUserId(String token) {
        LinkedHashMap user = getUser(token);
        return (String) user.get("id");
    }
}
