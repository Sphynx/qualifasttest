package com.qualifast.constant;

public class ApiPath {

    public static final String CALLBACK = "/callback";
    public static final String LOGOUT = "/logout";
    public static final String ERROR = "/error";
    public static final String RECENT_TRACKS = "/recentTracks";
    public static final String REDIRECT = "/redirect";
}
