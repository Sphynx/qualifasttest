package com.qualifast.repository;

import com.qualifast.entity.Song;
import java.time.LocalDateTime;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SongRepository extends JpaRepository<Song, Long> {

    Optional<Song> findBySpotifyUserIdAndSpotifySongIdAndPlayedAt(String spotifyUserId, String spotifySongId,
            LocalDateTime playedAt);
}
