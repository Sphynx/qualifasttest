package com.qualifast.entity;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@DynamicInsert
@DynamicUpdate
@Entity
@Table(name = "songs")
@NoArgsConstructor
@AllArgsConstructor
public class Song {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String spotifyArtistId;
    private String spotifyArtistName;
    private String spotifyUserId;
    private String spotifySongId;

    private LocalDateTime playedAt;
}

/*
CREATE TABLE songs (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    spotify_artist_id VARCHAR(100) NOT NULL,
    spotify_artist_name VARCHAR(255) NOT NULL,
    spotify_user_id VARCHAR(100) NOT NULL,
    spotify_song_id VARCHAR(100) NOT NULL,
    played_at TIMESTAMP
);
 */
