package com.qualifast.controller;

import com.qualifast.constant.ApiPath;
import com.qualifast.constant.Template;
import com.qualifast.service.AccessTokenService;
import com.qualifast.service.ProfileDetailService;
import com.qualifast.service.SpotifyUrlService;
import javax.servlet.http.HttpSession;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@AllArgsConstructor
public class CallbackController {

    private final SpotifyUrlService spotifyUrlService;
    private final AccessTokenService accessToken;
    private final ProfileDetailService userDetails;

    @GetMapping(value = ApiPath.CALLBACK, produces = MediaType.TEXT_HTML_VALUE)
    public String handleCallback(@RequestParam(value = "code", required = false) final String code,
            @RequestParam(value = "error", required = false) final String error, final Model model,
            final HttpSession session) {

        if (error != null) {
            model.addAttribute("url", spotifyUrlService.getAuthorizationURL());
            return Template.CALLBACK_FAILURE;
        }
        session.setAttribute("code", code);
        String token = accessToken.getToken(code);

        session.setAttribute("accessToken", token);
        model.addAttribute("accessToken", token);
        model.addAttribute("userName", userDetails.getUsername(token));

        return Template.CALLBACK_SUCCESS;
    }
}
