package com.qualifast.controller;

import com.qualifast.constant.ApiPath;
import com.qualifast.constant.Template;
import com.qualifast.service.RecentPlayedTrackService;
import javax.servlet.http.HttpSession;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@AllArgsConstructor
public class MusicController {

    private final RecentPlayedTrackService recentPlayedTrackService;

    @GetMapping(value = ApiPath.RECENT_TRACKS, produces = MediaType.TEXT_HTML_VALUE)
    public String recentTracksHandler(final HttpSession session, final Model model) {
        model.addAttribute("tracks", recentPlayedTrackService.getHistory((String) session.getAttribute("accessToken")));
        return Template.RECENT_TRACKS;
    }
}
