package com.qualifast.controller;

import com.qualifast.constant.ApiPath;
import com.qualifast.constant.Template;
import com.qualifast.service.ProfileDetailService;
import javax.servlet.http.HttpSession;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@AllArgsConstructor
public class RedirectController {

    private final ProfileDetailService userDetails;

    @GetMapping(value = ApiPath.REDIRECT, produces = MediaType.TEXT_HTML_VALUE)
    public String redirectToCallbackSuccess(final HttpSession session, final Model model) {

        String token = (String) session.getAttribute("accessToken");
        model.addAttribute("accessToken", token);
        model.addAttribute("userName", userDetails.getUsername(token));

        return Template.CALLBACK_SUCCESS;
    }

}
