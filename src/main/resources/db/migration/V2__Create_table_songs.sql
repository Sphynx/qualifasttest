CREATE TABLE songs (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    spotify_artist_id VARCHAR(100) NOT NULL,
    spotify_artist_name VARCHAR(255) NOT NULL,
    spotify_user_id VARCHAR(100) NOT NULL,
    spotify_song_id VARCHAR(100) NOT NULL,
    played_at TIMESTAMP
);